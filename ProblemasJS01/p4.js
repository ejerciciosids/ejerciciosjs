function nueva() {
    let people = [{ name: "bob", id: 1 },
    { name: "john", id: 2 },
    { name: "alex", id: 3 },
    { name: "alex", id: 3 },
    { name: "john", id: 3 }];
    let nuevo = [];

    for (i = 0; i < people.length; i++) {
        nuevo.push(people[i].name); //agregar a nuevo arreglo
    }

    nuevo = nuevo.sort(); //ordenar

    let unicos = [];
    let vecesRepetidas = [];
    let contar = 1;

    for (let i = 0; i < nuevo.length; i++) {
        if (nuevo[i + 1] === nuevo[i]) {
            //duplicados.push(nuevo[i]);
            contar++;
        } else {
            unicos.push(nuevo[i]);
            vecesRepetidas.push(contar);
            contar = 1;
        }
    }
    console.log('unicos elementos ', unicos);//elementos no repetidos
    console.log('Veces Repetidas: ', vecesRepetidas);


    for (let j = 0; j < unicos.length; j++) {
        console.log("El valor " + unicos[j] + " se repite " + vecesRepetidas[j])
    }
}
nueva();