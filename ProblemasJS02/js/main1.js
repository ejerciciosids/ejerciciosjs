var personArr = [
    {
        "personId": 123,
        "name": "Jhon",
        "city": "Melbourne",
        "phoneNo": "1234567890"
    },
    {
        "personId": 124,
        "name": "Amelia",
        "city": "Sydney",
        "phoneNo": "1234567890"
    },
    {
        "personId": 125,
        "name": "Emily",
        "city": "Perth",
        "phoneNo": "1234567890"
    },
    {
        "personId": 126,
        "name": "Abraham",
        "city": "Perth",
        "phoneNo": "1234567890"
    }
];
const bodytabla = document.querySelector('#cuerpoTabla');
var datos = personArr.map((dato) => {
    const tr = document.createElement('tr');
        const td1 = document.createElement('td');

        const td2 = document.createElement('td');
        const td3 = document.createElement('td');
        const td4 = document.createElement('td');

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4);

        td1.textContent = dato.personId
        td2.textContent = dato.name
        td3.textContent = dato.city;
        td4.textContent = dato.phoneNo;

        bodytabla.appendChild(tr);
})
