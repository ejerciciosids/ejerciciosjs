function principal() {
    function foo() {
        var a = 'hello';
        function bar() {
            var b = 'world';
            console.log(a);
            console.log(b);
        }
        bar();
        console.log(a); 
        /*console.log(b); ERROR*/

    }
    /*console.log(a); ERROR
    console.log(b);*/
    foo();
}
principal();
