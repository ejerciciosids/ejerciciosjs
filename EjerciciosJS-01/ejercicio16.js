let age =18
let height =4;
let status2 = 'royalty';
let hasInvitation =false;

var isLegal = age>=18;
console.log(isLegal);
var tall = height>=5.11;
console.log(tall);
var suitable = isLegal && tall;
console.log(suitable);
var isRoyalty = status2 === 'royalty';
console.log(isRoyalty);
var specialCase = isRoyalty && hasInvitation;
console.log(specialCase);
var canEnterourBar = suitable || specialCase;
console.log(canEnterourBar);