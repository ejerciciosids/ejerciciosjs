var a = 'hello' && '';
console.log(a);
console.log(typeof a);
var b  = '' && [];
console.log(b);
console.log(typeof b);

var c = undefined && 0;
console.log(c);


var d = 1 && 5;
console.log(d);
console.log(typeof d);
var e = 0 && {};
console.log(e);
console.log(typeof e);

var f = 'hi' && [] && 'done';
console.log(f)
console.log(typeof f);

var g = 'bye' && undefined && 'adios';
console.log(g);

