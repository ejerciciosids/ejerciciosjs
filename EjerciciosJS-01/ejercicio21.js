function principal() {
    function foo() {
        const a = true;

        function bar() {
            const a = false;
            console.log(a);
        }
        //const a = false; //no se puede redeclarar porque ya esta
        //a = false; //marca error
        console.log(a);
        bar();
    }
    foo();
}
principal();