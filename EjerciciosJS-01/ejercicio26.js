function letras(){

    var array =['a','b','c','d','e','f'];
    //Estos metodo modifican el array original
    array.copyWithin(5,0,1);
    console.log(array);
    array.copyWithin(3,0,3);
    console.log(array);
    array.fill('z', 0, 5);
    console.log(array);
}
function nombres(){
    var array = ['Alberto',  'Mauricio', 'Ana', 'Bernardo', 'Zoe'];
    //OJO: cada línea está modificando el array original
    array.sort();
    console.log(array);
    array.reverse();
    console.log(array);

}
letras();
nombres();