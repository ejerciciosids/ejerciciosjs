var x=5+7;
console.log(x);//12
console.log(typeof x); //number

var x=5+"7";
console.log(x);// "57"
console.log(typeof x); //string

var x="5"+7;
console.log(x); //"57"
console.log(typeof x); //string

var x=5-7;
console.log(x);//-2
console.log(typeof x);//number

var x=5-"7";
console.log(x);//-2
console.log(typeof x);//number

var x="5"-7;
console.log(x);//-2
console.log(typeof x); //number

var x=5-"x";
console.log(x);//NaN
console.log(typeof x); //number


