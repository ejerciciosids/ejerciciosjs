//-------Conocimientos generales 
//5¿Cuál es la forma correcta de definir una lista de objetos de ciudades en formato JSON:  
let lista = `{"ciudades":[
    {"nombre": "guadalajara", "sigla":"GDL" },
    {"nombre": "Monterrey", "sigla":"MTY" },
    {"nombre": "Guanajuato", "sigla":"GTO" }
]
}`
x = JSON.parse(lista);
//console.log(x)



//------------------Javascript
//1.-
const employee ={
    firstname: 'pedro',
    age: 30,
}
const street = employee.address?.street
console.log(street); //undefined

//2.-
const employee2 ={
    firstname: 'pedro',
    age: 30,
}
//const street2 = employee2.address.street2;
//console.log(street2);//TypeError: Cannot read properties of undefined (reading 'street2')


/*---- 3.- 
function resolveAfter2Seconds() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('resolved');
    }, 2000);
  });
}

async function asyncCall() {
  console.log('calling');
  const result = await resolveAfter2Seconds();
  console.log(result);
  // expected output: "resolved"
}

asyncCall();*/


// 5.-
var first = [1,2,3,4,5];
var second = [1,2,3,4];
var isEqual = first.length === second.length && first.every((value, index)=> value===second[index])
console.log(isEqual) //false

// 6.- 
let condicion1, resultado, condicion2;
condicion1 = 2>8;
condicion2 = 8>2;
resultado = condicion1 && condicion2;
console.log(resultado);//false


/* 7.-
const promise1 = () =>{
  return new Promise((resolve, reject)=>{setTimeout(()=> resolve('promise1 fulfilled'), 2000);});
};
const promise2 = () =>{
  return new Promise((resolve, reject)=>{setTimeout(()=> resolve('promise2 fulfilled'), 1000);});
};
promise1().then((result)=> console.log(result));
promise2().then((result)=> console.log(result));
//resultado   promise2 fulfilled
              promise1 fulfilled*/


// 8 .- 
const array = [21,13,34,26,3,5];
const foundNumber = array.find(number=>number>20);
console.log(foundNumber); //21

//9 .-
aNum=[5,4,3,2,1,0]
aNum.reverse();
console.log(aNum); //[ 0, 1, 2, 3, 4, 5 ]
//10 .-
let message = 'hello';
alert(typeof message); // string
