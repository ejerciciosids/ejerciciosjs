import { html, css, LitElement, nothing } from 'lit';

export class MiwebComponent extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--miweb-component-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      name: {
        type: String,
        attribute: 'fullname'
      },
      persons: { type: Array },
      colors: { type: Array },
      condition: { type: Boolean}
    };
  }

  constructor() {
    super();
    this.colors = ['Rojo', 'Verde', 'Negro', 'Azul'];
    this.condition = false;
  }


  render() {
    return html`
    <ul>
      ${this.colors.map((color)=>html`
      <li style="color: ${color}">${color}</li>
      `
        )}
    </ul>
    <button @click="${this.metodo}">click</button>
    ${this.condition? html`<label>condition true</label>`: nothing};



     
    `;
  }
  metodo(){
    const event = new CustomEvent('my-event', {detail:{message: 'Something happened'}})
    this.dispatchEvent(event);
    console.log(event)
  }
}
