const section = document.querySelector('section');
const bodytabla = document.querySelector('tbody');

const requestURL = 'https://api.datos.gob.mx/v1/calidadAire';
const request = new XMLHttpRequest();

request.open('GET', requestURL);

request.responseType = 'text';
request.send();


const results = 'results'
request.onload = function () {
    const superHeroesText = request.response;
    const superHeroes = JSON.parse(superHeroesText);
    //console.log(superHeroes);
    populateHeader(superHeroes);
    
}
function populateHeader(jsonObj) {
    const datos = jsonObj['results'];

    for (let i = 0; i < datos.length; i++) {
        const tr = document.createElement('tr');
        tr.className = 'articulo';
        const td1 = document.createElement('td');

        const td2 = document.createElement('td');
        const td3 = document.createElement('td');
        const td4 = document.createElement('td');


        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4);

        td1.textContent = datos[i]['stations'][0].id
        td2.textContent = datos[i]['stations'][0].name;
        td3.textContent = datos[i]['stations'][0]['indexes'][0].value;
        td4.textContent = datos[i]['stations'][0]['indexes'][0].scale;

        bodytabla.appendChild(tr);

    }
}

document.addEventListener("keyup", e => {
    if (e.target.matches('#buscar')) {
        document.querySelectorAll('.articulo').forEach(nombre => {
            nombre.textContent.toLowerCase().includes(e.target.value.toLowerCase()) ? nombre.classList.remove('filtro') : nombre.classList.add('filtro')
        })
    }
})
function borrar() {
    document.getElementById('buscar').value ="";
    request.onload();   
}


/*const requestURL= 'https://api.datos.gob.mx/v1/calidadAire';

fetch(`${requestURL}`)
.then(response => {
return console.log(response.json());
})
.then ( response =>{
consol.log(response);
//o mándalo a alguna function que procese la respuesta
})
.catch( error => {
//hacer algo si falla la promesa
});*/