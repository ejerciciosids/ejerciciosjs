/*
 * 1.Al mover el ratón en cualquier punto de la ventana del navegador, 
 * se muestre la posición del puntero respecto del navegador y respecto de la página.
 * 2. Al pulsar cualquier tecla, el mensaje mostrado debe cambiar para indicar el
nuevo evento y su información asociada:
 */
{
    let generateMessages;

    function inicio() {
        mensaje = document.getElementById('informacion');
        document.addEventListener('mousemove', generateMessages);
        document.addEventListener('keypress', generateMessages);
    }

    generateMessages = function (ev) {
        let relativaX, relativaY, absolutaX, absolutaY;
        absolutaX = ev.pageX;
        absolutaY = ev.pageY;
        relativaX = ev.clientX;
        relativaY = ev.clientY;

        switch (ev.type) {
            case 'mousemove':
                mensaje.style.backgroundColor = 'white';
                mostrarInfo([
                    'Ratón',
                    'Posición en navegador: [' + relativaX + ', ' + relativaY + ']',
                    'Posición en página: [' + absolutaX + ', ' + absolutaY + ']'
                ]);
                break;
            case 'keypress':
                mensaje.style.backgroundColor = '#DAF7A6';
                let character = ev.charCode || ev.keyCode;
                let sign = String.fromCharCode(character);
                let code = sign.charCodeAt(0);
                mostrarInfo([
                    'Pulsando el teclado:',
                    'Carácter de la tecla pulsada: [' + sign + ']',
                    'Código del carácter: [' + code + ']'
                ]);
                break;
        }
    };

    let mostrarInfo = function (info) {
        mensaje.innerHTML = '<h2>' + info[0] + '</h2>';
        for (let i = 1; i < info.length; i++) {
            mensaje.innerHTML += '<p>' + info[i] + '</p>';
        }
    };

    document.addEventListener('DOMContentLoaded', inicio);
}
